# README #
This project tends to use probabilistic model to select the emotion information from image. 

The steps of using this project:
1. download images from website, using ImageDownLoad.java
2. use Matlab project to process the images, and extract features. 
3. process features, using PictureFeatureProcess.java
4. process the document, use DocumentProcess.java to combine all the features. 