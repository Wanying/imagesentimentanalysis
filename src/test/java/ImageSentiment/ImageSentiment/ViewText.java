package ImageSentiment.ImageSentiment;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Random;
import java.util.Scanner;

public class ViewText {
	//document is to long to view, use this to view
	public static void main(String args[]) throws IOException{
		String fileAddress="D:/Projects/ImageProject/ImageSentiment/dataset/text/data.txt";
		String outputFile="D:/Projects/ImageProject/ImageSentiment/dataset/text/test.txt";
		
		File outf = new File(outputFile);
		FileWriter fw = new FileWriter(outf);
		BufferedWriter bw = new BufferedWriter(fw);
		
		File file = new File(fileAddress);
		FileReader fr = new FileReader(file);
		Scanner scanner = new Scanner(fr);
		int n=0;
		while(scanner.hasNext()){
			String line = scanner.nextLine().trim();
			n++;
			System.out.println("N: "+n);
			if(n%20==0){
				bw.write(line);
				bw.newLine();
			}	
		}
		bw.flush();
		bw.close();
	}
	

}
