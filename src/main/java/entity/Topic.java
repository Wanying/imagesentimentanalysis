package entity;


/*
 * This class is used to identify the entity of topic 
 * */
public class Topic {
	public int topicIndex;
	public int[] textualTopicList;
	public int[] visualTopicList;
	public int[] textualWordList;
	public int[] visualWordList;
	
	public int numberOfTextualTopic;
	public int numberOfVisualTopic;
	public int numberOfTextualWord;
	public int numberOfVisualWord;
	
	public Topic(int topicIndex, int numberOfTextualTopic, int numberOfVisualTopic,int numberOfTextualWord, int numberOfVisualWord){
		this.topicIndex=topicIndex;
		this.textualTopicList = new int[numberOfTextualTopic];
		this.visualTopicList= new int[numberOfVisualTopic];
		this.textualWordList = new int[numberOfTextualWord];
		this.visualWordList = new int[numberOfVisualWord];
		this.numberOfTextualTopic=0;
		this.numberOfVisualTopic=0;
		this.numberOfTextualWord=0;
		this.numberOfVisualWord=0;
	}
}
