package entity;

public class TextualEmotion {
	public int emotionIndex;
	public int[] wordList;
	public int numberOfWord;
	public int emotionAssignment;
	
	public TextualEmotion(int emotionIndex,int numberOfTextualWord){
		this.emotionIndex=emotionIndex;
		this.wordList = new int[numberOfTextualWord];
		this.numberOfWord=0;
		this.emotionAssignment=-1;
	}

}
