package entity;

import java.util.ArrayList;

import constants.Constants;

/*
 * This class is used to model the document 
 * */
public class Document {
	
	public int documentIndex;
	
	public int textLength;
	public int visualLength;
	
	public int numberOfTextualTopicWord;
	public int numberOfTextualEmotionWord;
	public int numberOfVisualTopicWord;
	public int numberOfVisualEmotionWord;
	
	public ArrayList<Word> textualWordList;
	public ArrayList<Word> visualWordList;
	
	public int[] textualTopicList;
	public int[] textualEmotionList;
	public int[] visualTopicList;
	public int[] visualEmotionList;
	

	
	public Document(int documentIndex, int numberOfTextualTopic, int numberOfVisualTopic, int numberOfTextualEmotion, int numberOfVisualEmotion){
		this.documentIndex=documentIndex;
		
		this.textualTopicList = new int[numberOfTextualTopic];
		this.textualEmotionList = new int[numberOfTextualEmotion];
		this.visualTopicList= new int[numberOfVisualTopic];
		this.visualEmotionList = new int[numberOfVisualEmotion];
		
		this.textualWordList = new ArrayList<Word>();
		this.visualWordList = new ArrayList<Word>();
		
		this.numberOfTextualTopicWord=0;
		this.numberOfTextualEmotionWord=0;
		this.numberOfVisualTopicWord=0;
		this.numberOfVisualEmotionWord=0;
	}
	
	//for test
	public static void main(String args[]){
	}

}
