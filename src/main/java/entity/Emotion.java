package entity;

public class Emotion {
	
	public int emotionIndex;
	public int[] textualEmotionList;
	public int[] visualEmotionList;
	public int[] textualWordList;
	public int[] visualWordList;
	
	public int numberOfTextualEmotion;
	public int numberOfVisualEmotion;
	public int numberOfTextualWord;
	public int numberOfVisualWord;
	
	public Emotion(int emotionIndex,int numberOfTextualEmotion, int numberOfVisualEmotion, int numberOfTextualWord, int numberOfVisualWord){
		this.emotionIndex=emotionIndex;
		this.textualEmotionList=new int[numberOfTextualEmotion];
		this.visualEmotionList=new int[numberOfVisualEmotion];
		this.textualWordList = new int[numberOfTextualWord];
		this.visualWordList = new int[numberOfVisualWord];
		
		this.numberOfTextualEmotion=0;
		this.numberOfVisualEmotion=0;
		this.numberOfTextualWord=0;
		this.numberOfVisualWord=0;
	}

}
