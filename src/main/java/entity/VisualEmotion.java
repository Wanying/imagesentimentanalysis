package entity;

public class VisualEmotion {
	public int emotionIndex;
	public int[] wordList;
	public int numberOfWord;
	public int emotionAssignment;
	
	public VisualEmotion(int emotionIndex, int numberOfVisualWord){
		this.emotionIndex=emotionIndex;
		this.wordList= new int[numberOfVisualWord];
		this.numberOfWord=0;
		this.emotionAssignment=-1;
	}
}
