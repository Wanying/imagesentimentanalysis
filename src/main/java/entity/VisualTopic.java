package entity;

public class VisualTopic {
	public int topicIndex;
	public int[] wordList;
	public int numberOfWord;
	public int topicAssignment;
	
	public VisualTopic(int topicIndex, int numberOfVisualWord){
		this.topicIndex=topicIndex;
		this.wordList= new int[numberOfVisualWord];
		this.numberOfWord=0;
		this.topicAssignment=-1;
	}
}
