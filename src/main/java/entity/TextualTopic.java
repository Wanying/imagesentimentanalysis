package entity;

public class TextualTopic {
	public int topicIndex;
	public int[] wordList;
	public int numberOfWord;
	public int topicAssignment;
	
	public TextualTopic(int topicIndex,int numberOfTextualWord){
		this.topicIndex=topicIndex;
		this.wordList = new int[numberOfTextualWord];
		this.numberOfWord=0;
		this.topicAssignment=-1;
		
	}

}
