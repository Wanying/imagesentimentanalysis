/**
 * 
 */
package entity;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Scanner;

import constants.Constants;

/**
 * @author TCL
 *
 */
public class DataProcess {

	/**
	 * @param args
	 */
	
	private ArrayList<String> documentList;
	private int numberOfDocument;
	private int numberOfTextualWord;
	private int numberOfVisualWord;
	private Document[] documents;
	
	private HashSet<String> stopSet = new HashSet<String>();
	private HashSet<String> emotionSet = new HashSet<String>();
	
	private HashMap<Integer,String> ID2W = new HashMap<Integer,String>();
	private HashMap<String,Integer> W2ID = new HashMap<String,Integer>();
	private HashMap<Integer,String> ID2V = new HashMap<Integer,String>();
	private HashMap<String,Integer> V2ID = new HashMap<String,Integer>();
	
	public DataProcess(String documentFile,String stopFile,String emotionFile) throws IOException{
		readFile(documentFile);
		setDocumentAndDictionary(stopFile,emotionFile);
	}
	
	private void setStopSet(String stopFile) throws IOException{
		File inf = new File(stopFile);
		FileReader fr = new FileReader(inf);
		Scanner scanner = new Scanner(fr);
		while(scanner.hasNext()){
			String line = scanner.nextLine().trim();
			stopSet.add(line);
		}
	}
	
	private void setEmotionSet(String emotionFile) throws IOException{
		File inf = new File(emotionFile);
		FileReader fr = new FileReader(inf);
		Scanner scanner = new Scanner(fr);
		while(scanner.hasNext()){
			String line = scanner.nextLine().trim();
			emotionSet.add(line);
		}
	}
	private void readFile(String documentFile) throws IOException{
		this.documentList = new ArrayList<String>();
		File inf = new File(documentFile);
		FileReader fr = new FileReader(inf);
		Scanner scanner = new Scanner(fr);
		int n=0;
		while(scanner.hasNext()){
			String line = scanner.nextLine().trim();
			documentList.add(line);
		}
		this.numberOfDocument=documentList.size();
	}
	
	private void setDocumentAndDictionary(String stopFile, String emotionFile) throws IOException{
		
		int wid=0;
		int vid=0;
		this.documents = new Document[numberOfDocument];
		setStopSet(stopFile);
		setEmotionSet(emotionFile);
		
		for(int d=0;d<documentList.size();d++){
			String docLine = documentList.get(d).trim();
			String segments[] = docLine.split(":");
			
			String idstr = segments[0].trim();
			int id = Integer.parseInt(idstr.trim());
			String content = segments[1].trim();
			
			String terms[] = content.split(";");
			String textTerm = terms[0].trim();
			String visualTerm ="";
			try{
				visualTerm=terms[1].trim();
			}catch(ArrayIndexOutOfBoundsException e){
				visualTerm="";
			}
			
			String textEntities[] = textTerm.split(" ");
			String visualEntities[] = visualTerm.split(" ");
			
			Document doc = new Document(id,Constants.numberOfTextualTopic,Constants.numberOfVisualTopic,Constants.numberOfTexualEmotion,Constants.numberOfVisualEmotion);
			//process the textual part
			for(int i=0;i<textEntities.length;i++){
				String textStr = textEntities[i].trim();
				if(textStr.isEmpty()) continue;
				if(textStr.length()<=2) continue;
				if(stopSet.contains(textStr)) continue;
				
				if(!(W2ID.containsKey(textStr))){
					W2ID.put(textStr, wid);
					ID2W.put(wid, textStr);
					wid++;
				}
				int wordIndex = W2ID.get(textStr);
				Word word = new Word(wordIndex);
				if(emotionSet.contains(textStr)){
					word.wordAttr=1;
					doc.numberOfTextualEmotionWord++;
				}else{
					word.wordAttr=0;
					doc.numberOfTextualTopicWord++;
				}
				doc.textualWordList.add(word);
			}
			
			for(int j=0;j<visualEntities.length;j++){
				String visualStr = visualEntities[j].trim();
				if(visualStr.isEmpty()) continue;
				if(!(V2ID.containsKey(visualStr))){
					V2ID.put(visualStr, vid);
					ID2V.put(vid, visualStr);
					vid++;
				}
				int wordIndex = V2ID.get(visualStr);
				Word word = new Word(wordIndex);
				if((visualStr.contains("color"))||(visualStr.contains("texton")||visualStr.contains("lbp"))){
					word.wordAttr=1;
					doc.numberOfVisualEmotionWord++;
				}else{
					word.wordAttr=0;
					doc.numberOfVisualTopicWord++;
				}
				doc.visualWordList.add(word);
			}
			doc.textLength=doc.textualWordList.size();
			doc.visualLength=doc.visualWordList.size();
			documents[d]=doc;
		}
		this.numberOfTextualWord=W2ID.size();
		this.numberOfVisualWord=V2ID.size();
	}
	
	
	public Document[] getDocuments(){
		return documents;
	}
	
	public int getNumberOfDocument(){
		return numberOfDocument;
	}
	
	public int getNumberOfTextualWord(){
		return numberOfTextualWord;
	}
	
	public int getNumberOfVisualWord(){
		return numberOfVisualWord;
	}
	public HashMap<Integer,String> getID2W(){
		return this.ID2W;
	}
	public HashMap<String,Integer> getW2ID(){
		return W2ID;
	}
	public HashMap<Integer,String> getID2V(){
		return this.ID2V;
	}
	public HashMap<String,Integer> getV2ID(){
		return this.V2ID;
	}

}
