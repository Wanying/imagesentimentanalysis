package entity;

/*
 * This class is used for identifying the entity of Word
 * */
public class Word {
	
	//word Index in the dictionary
	public int wordIndex;
	//topic word: wordAttr=0; sentiment word: wordAttr=1
	public int wordAttr;
	
	
	public int textualTopicAssignment;
	public int visualTopicAssignment;
	public int textualEmotionAssignment;
	public int visualEmotionAssignment;
	public int topicAssignment;
	public int emotionAssignment;
	
	public Word(int wordIndex){
		this.wordIndex=wordIndex;
		this.wordAttr=-1;
		
		this.textualTopicAssignment=-1;
		this.visualTopicAssignment=-1;
		this.textualEmotionAssignment=-1;
		this.visualEmotionAssignment=-1;
		
		this.topicAssignment=-1;
		this.emotionAssignment=-1;
	}
	
	public boolean equals(Object obj){
		if((((Word)obj).wordIndex==this.wordIndex)&&(((Word)obj).wordAttr==this.wordAttr))
			return true;
		else 
			return false;
	}
	public int hashCode(){
		int result=17;
		result+=this.wordIndex*31;
		result+=this.wordAttr*31;	
		return result;
	}
}
