package algorithm;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Scanner;

import entity.Document;
import entity.Emotion;
import entity.TextualEmotion;
import entity.TextualTopic;
import entity.Topic;
import entity.VisualEmotion;
import entity.VisualTopic;
import entity.Word;

public class Evaluation {
	
	public int numberOfDocument;
	public int numberOfTextualWord;
	public int numberOfVisualWord;
	public int numberOfTextualTopic;
	public int numberOfVisualTopic;
	public int numberOfTextualEmotion;
	public int numberOfVisualEmotion;
	
	public int numberOfTopic;
	public int numberOfEmotion;
	
	public Document[] documents;
	public Topic[] topicList;
	public Emotion[] emotionList;
	public TextualTopic[] textualTopicList;
	public VisualTopic[] visualTopicList;
	public TextualEmotion[] textualEmotionList;
	public VisualEmotion[] visualEmotionList;
	
	public HashMap<Integer,String> ID2W;
	public HashMap<Integer,String> ID2V;
	public HashMap<String,Integer> W2ID;
	public HashMap<String,Integer> V2ID;
	
	public Evaluation(GibbsSampling gs){
		init(gs);
	}
	
	public void init(GibbsSampling gs){
		this.numberOfDocument = gs.numberOfDocument;
		this.numberOfTextualWord = gs.numberOfTextualWord;
		this.numberOfVisualWord = gs.numberOfVisualWord;
		this.numberOfTextualTopic = gs.numberOfTextualTopic;
		this.numberOfVisualTopic = gs.numberOfVisualTopic;
		this.numberOfTextualEmotion = gs.numberOfTextualEmotion;
		this.numberOfVisualEmotion = gs.numberOfVisualEmotion;
		
		this.numberOfTopic=gs.numberOfTopic;
		this.numberOfEmotion = gs.numberOfEmotion;
		
		this.documents=gs.documents;
		this.topicList = gs.topicList;
		this.emotionList = gs.emotionList;
		this.textualTopicList=gs.textualTopicList;
		this.textualEmotionList = gs.textualEmotionList;
		this.visualTopicList=gs.visualTopicList;
		this.visualEmotionList =gs.visualEmotionList;
		
		this.ID2W = gs.model.ID2W;
		this.ID2V = gs.model.ID2V;
		this.W2ID = gs.model.W2ID;
		this.V2ID = gs.model.V2ID;
		
	}
	
	public void getAccuracy(int threshold,String documentFile,String topicFile, String emotionFile,String accuracyFile) throws IOException{
		
		HashMap<Integer,HashSet<String>> docTopicMap = new HashMap<Integer,HashSet<String>>();
		HashMap<Integer,HashSet<String>> docEmotionMap = new HashMap<Integer,HashSet<String>>();
		for(int d=0;d<documents.length;d++){
			Document document = documents[d];
			int documentIndex = document.documentIndex;
			ArrayList<Word> wordList = document.textualWordList;
			// the word set in this document;
			HashSet<String> twordSet = new HashSet<String>();
			HashSet<String> ewordSet = new HashSet<String>();
			for(int w=0;w<wordList.size();w++){
				Word word = wordList.get(w);
				String wordStr = ID2W.get(word.wordIndex).trim();
				if(word.wordAttr==0){
					twordSet.add(wordStr);
				}else{
					ewordSet.add(wordStr);
				}
			}
			docTopicMap.put(documentIndex,twordSet);
			docEmotionMap.put(documentIndex, ewordSet);
		}
		
		//read the document File
		HashMap<Integer,ArrayList<Integer>> docTopic = new HashMap<Integer,ArrayList<Integer>>();
		HashMap<Integer,ArrayList<Integer>> docEmotion = new HashMap<Integer,ArrayList<Integer>>();
		
		File docFile = new File(documentFile);
		FileReader docFr= new FileReader(docFile);
		Scanner scanner = new Scanner(docFr);
		while(scanner.hasNext()){
			String line = scanner.nextLine().trim();
			String terms[] = line.split(" ");
			int id = Integer.parseInt(terms[0].trim());
			String type = terms[1].trim();
			String listStr = terms[2].trim();
			String[] list = listStr.split("\t");
			if(type.contains("Topic")){
				ArrayList<Integer> docTopicList = new ArrayList<Integer>();
				int topic1 = Integer.parseInt(list[0].substring(0,list[0].indexOf("-")).trim());
				int topic2 = Integer.parseInt(list[1].substring(0,list[1].indexOf("-")).trim());
				int topic3 = Integer.parseInt(list[2].substring(0,list[2].indexOf("-")).trim());
				docTopicList.add(topic1);
				docTopicList.add(topic2);
				docTopicList.add(topic3);
				docTopic.put(id,docTopicList);
			}else if(type.contains("Emotion")){
				ArrayList<Integer> docEmotionList = new ArrayList<Integer>();
				int emotion1 = Integer.parseInt(list[0].substring(0,list[0].indexOf("-")).trim());
				int emotion2 = Integer.parseInt(list[1].substring(0,list[1].indexOf("-")).trim());
				int emotion3 = Integer.parseInt(list[2].substring(0,list[2].indexOf("-")).trim());
				docEmotionList.add(emotion1);
				docEmotionList.add(emotion2);
				docEmotionList.add(emotion3);
				docEmotion.put(id, docEmotionList);
			}
		}
		
		//read Topic File
		HashMap<Integer,HashSet<String>> topicMap = new HashMap<Integer,HashSet<String>>();
		File tFile = new File(topicFile);
		FileReader topicFR = new FileReader(tFile);
		Scanner topicScanner = new Scanner(topicFR);
		
		int topicID=-1;
		HashSet<String> topicWordSet = new HashSet<String>();
		while(topicScanner.hasNext()){
			String line = topicScanner.nextLine().trim();
			
			if(line.contains("Topic")) {
				if(!(topicWordSet.isEmpty())){
					topicMap.put(topicID, topicWordSet);
				}
				topicID++;
				topicWordSet = new HashSet<String>();
				continue;
			}
			String terms[]=  line.split(":");
			String word = terms[0].trim();
			topicWordSet.add(word);
		}
		topicMap.put(topicID, topicWordSet);
		
		//read Emotion File
		HashMap<Integer,HashSet<String>> emotionMap = new HashMap<Integer,HashSet<String>>();
		File eFile = new File(emotionFile);
		FileReader emotionFR = new FileReader(eFile);
		Scanner emotionScanner = new Scanner(emotionFR);
		
		int emotionID=-1;
		HashSet<String> emotionWordSet = new HashSet<String>();
		while(emotionScanner.hasNext()){
			String line = emotionScanner.nextLine().trim();
			
			if(line.contains("Emotion")){
				if(!(emotionWordSet.isEmpty())){
					emotionMap.put(emotionID, emotionWordSet);
				}
				emotionID++;
				emotionWordSet= new HashSet<String>();
				continue;
			}
			String terms[] = line.split(":");
			String word = terms[0].trim();
			emotionWordSet.add(word);
		}
		emotionMap.put(emotionID, emotionWordSet);
		
		//calculate the accuracy
		int docTopicCount=0;
		int docEmotionCount=0;
		for(int d=0;d<documents.length;d++){
			//all the words in document
			int documentIndex = documents[d].documentIndex;
			HashSet tdocSet = docTopicMap.get(documentIndex);
			HashSet edocSet = docEmotionMap.get(documentIndex);
			
			//System.out.println(documentIndex+" : "+tdocSet.toString()+";"+edocSet);
			
			
			//get document topic
			ArrayList<Integer> docTopicList = docTopic.get(documentIndex);
			//System.out.println("document ID "+documentIndex+":"+docTopicList);
			
			HashSet<String> docTopicWordSet = new HashSet<String>();
			docTopicWordSet.addAll(topicMap.get(docTopicList.get(0)));
			docTopicWordSet.addAll(topicMap.get(docTopicList.get(1)));
			docTopicWordSet.addAll(topicMap.get(docTopicList.get(2)));
			
			//System.out.println(documentIndex+"-Document Words: "+tdocSet);
			//System.out.println("Topic Words: "+docTopicList.get(0)+": "+topicMap.get(docTopicList.get(0)));
			
			int topicWordCount=0;
			int emotionWordCount=0;
			
			docTopicWordSet.retainAll(tdocSet);
			
			int n;
			if((tdocSet.size()/2)<threshold){
				n=tdocSet.size()/2;
			}else{
				n=5;
			}
			
			
			if(docTopicWordSet.size()>=n) docTopicCount++;
			topicWordCount+=docTopicWordSet.size();
			
			//get document emotion
			ArrayList<Integer> docEmotionList = docEmotion.get(documentIndex);
			HashSet<String> docEmotionWordSet = new HashSet<String>();
			docEmotionWordSet.addAll(emotionMap.get(docEmotionList.get(0)));
			docEmotionWordSet.addAll(emotionMap.get(docEmotionList.get(1)));
			docEmotionWordSet.addAll(emotionMap.get(docEmotionList.get(2)));
			
			docEmotionWordSet.retainAll(edocSet);
			
			int m=6;
			if((edocSet.size())/2<6){
				m=edocSet.size()/2;
			}else{
				m=6;
			}
			if(docEmotionWordSet.size()>=m) docEmotionCount++;
			emotionWordCount+=docEmotionWordSet.size();
			
		}
		
		File accFile = new File(accuracyFile);
		FileWriter fw = new FileWriter(accFile);
		BufferedWriter bw = new BufferedWriter(fw);
		double topicAcc = (double)docTopicCount/(double)documents.length;
		double emotionAcc =(double)docEmotionCount/(double)documents.length;
		
		bw.write("Topic Accuracy: "+docTopicCount+","+documents.length+","+topicAcc);
		bw.newLine();
		bw.write("Emotion Accuracy: "+docEmotionCount+","+documents.length+","+emotionAcc);
		bw.newLine();
		bw.flush();
		bw.close();
		
		System.out.println("Topic Accuracy: "+topicAcc+";"+numberOfTopic+"-"+numberOfEmotion);
		System.out.println("Emotion Accuracy: "+emotionAcc+";"+numberOfTopic+"-"+numberOfEmotion);
	}
	
}
