package algorithm;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

import entity.Document;
import entity.Emotion;
import entity.TextualEmotion;
import entity.TextualTopic;
import entity.Topic;
import entity.VisualEmotion;
import entity.VisualTopic;
import entity.Word;

public class ModelSave {
	
	public int numberOfDocument;
	public int numberOfTextualWord;
	public int numberOfVisualWord;
	public int numberOfTextualTopic;
	public int numberOfVisualTopic;
	public int numberOfTextualEmotion;
	public int numberOfVisualEmotion;
	
	public int numberOfTopic;
	public int numberOfEmotion;
	
	public Document[] documents;
	public Topic[] topicList;
	public Emotion[] emotionList;
	public TextualTopic[] textualTopicList;
	public VisualTopic[] visualTopicList;
	public TextualEmotion[] textualEmotionList;
	public VisualEmotion[] visualEmotionList;
	
	public HashMap<Integer,String> ID2W;
	public HashMap<Integer,String> ID2V;
	public HashMap<String,Integer> W2ID;
	public HashMap<String,Integer> V2ID;
	
	public ModelSave(GibbsSampling gs){
		init(gs);
	}
	
	public void init(GibbsSampling gs){
		this.numberOfDocument=gs.numberOfDocument;
		this.numberOfTextualWord=gs.numberOfTextualWord;
		this.numberOfVisualWord=gs.numberOfVisualWord;
		this.numberOfTextualTopic=gs.numberOfTextualTopic;
		this.numberOfVisualTopic = gs.numberOfVisualTopic;
		this.numberOfTextualEmotion = gs.numberOfTextualEmotion;
		this.numberOfVisualEmotion = gs.numberOfVisualEmotion;
		
		this.numberOfTopic = gs.numberOfTopic;
		this.numberOfEmotion = gs.numberOfEmotion;
		
		this.documents=gs.documents;
		this.topicList=gs.topicList;
		this.emotionList = gs.emotionList;
		this.textualTopicList=gs.textualTopicList;
		this.textualEmotionList=gs.textualEmotionList;
		this.visualTopicList=gs.visualTopicList;
		this.visualEmotionList=gs.visualEmotionList;
		
		this.ID2W = gs.model.ID2W;
		System.out.println("SIZE Of Word: "+ID2W.size());
		this.ID2V = gs.model.ID2V;
		
		this.W2ID =gs.model.W2ID;
		this.V2ID =gs.model.V2ID;	
	}
	
	public void saveDocuments(String documentFile) throws IOException{
		BufferedWriter writer = new BufferedWriter(new FileWriter(new File(documentFile)));
		for(int d=0;d<documents.length;d++){
			Document document = documents[d];
			//process the topic things
			int textLength= document.textLength;
			int visualLength = document.visualLength;
			int[] dtList = new int[topicList.length];
			int[] deList = new int[emotionList.length];
			for(int w=0;w<textLength;w++){
				Word word = document.textualWordList.get(w);
				int wordAttr = word.wordAttr;
				if(wordAttr==0){
					int topic = word.topicAssignment;
					dtList[topic]++;
				}else{
					int emotion = word.emotionAssignment;
					deList[emotion]++;
				}
			}
			for(int v=0;v<visualLength;v++){
				Word word = document.visualWordList.get(v);
				int wordAttr = word.wordAttr;
				if(wordAttr==0){
					int topic = word.topicAssignment;
					dtList[topic]++;
				}else{
					int emotion=word.emotionAssignment;
					deList[emotion]++;
				}
			}
			
			EntityScore[] topicES= new EntityScore[numberOfTopic];
			for(int t=0;t<numberOfTopic;t++){
				EntityScore es = new EntityScore(t,dtList[t]);
				topicES[t]=es;
			}
			Arrays.sort(topicES);
			
			EntityScore[] emotionES = new EntityScore[numberOfEmotion];
			for(int e=0;e<numberOfEmotion;e++){
				EntityScore es = new EntityScore(e,deList[e]);
				emotionES[e]=es;
			}
			Arrays.sort(emotionES);
			
			int length = document.textLength+document.visualLength;
			
			writer.write(document.documentIndex+" Topic ");
			for(int t=0;t<topicES.length;t++){
				double topicPro = (double)topicES[t].score/(double)length;
				writer.write(topicES[t].id+"-"+topicPro+"\t");
			}
			writer.newLine();
			writer.write(document.documentIndex+" Emotion ");
			for(int e=0;e<emotionES.length;e++){
				double emotionPro = (double)emotionES[e].score/(double)length;
				writer.write(emotionES[e].id+"-"+emotionPro+"\t");
			}
			writer.newLine();
			writer.flush();
			
		}
		writer.flush();
		writer.close();
	}
	/*
	 * Because vision feature could not provide us any clue of topic or emotion,
	 * so, here we just save the text information
	 * */
	public void saveTopic(String topicFile) throws IOException{
		BufferedWriter writer = new BufferedWriter(new FileWriter(new File(topicFile)));
		for(int t=0;t<topicList.length;t++){
			writer.write("Topic "+t+" : ");
			writer.newLine();
			Topic topic = topicList[t];
			int[] wordList = topic.textualWordList;
			EntityScore list[]=new EntityScore[wordList.length];
			for(int w=0;w<wordList.length;w++){
				int wordCount = wordList[w];
				double score = (double)wordCount/(double)topic.numberOfTextualWord;
				EntityScore es = new EntityScore(w,score);
				list[w]=es;
			}
			Arrays.sort(list);
			for(int w=0;w<50;w++){
				writer.write(ID2W.get(list[w].id)+" : "+list[w].score);
				writer.newLine();
				writer.flush();
			}
		}
		writer.flush();
		writer.close();
	}
	
	public void saveEmotion(String emotionFile) throws IOException{
		BufferedWriter writer = new BufferedWriter(new FileWriter(new File(emotionFile)));
		for(int e=0;e<emotionList.length;e++){
			writer.write("Emotion "+e+" : ");
			writer.newLine();
			Emotion emotion = emotionList[e];
			int[] emotionWordList = emotion.textualWordList;
			EntityScore[] list = new EntityScore[emotionWordList.length];
			for(int w=0;w<emotionWordList.length;w++){
				int wordCount = emotionWordList[w];
				double score = (double)wordCount/(double)emotion.numberOfTextualWord;
				EntityScore es = new EntityScore(w,score);
				list[w]=es;
			}
			Arrays.sort(list);
			for(int w=0;w<50;w++){
				writer.write(ID2W.get(list[w].id)+" : "+list[w].score);
				writer.newLine();
				writer.flush();
			}
			
		}
		writer.flush();
		writer.close();
		
	}
	
	public void saveTopicImage(String topicImageFile) throws IOException{
		String[] list = new String[numberOfTopic];
		for(int l=0;l<list.length;l++){
			String str="";
			list[l]=str;
		}
		
		for(int d=0;d<documents.length;d++){
			Document document = documents[d];
			ArrayList<Word> textualWordList = document.textualWordList;
			ArrayList<Word> visualWordList = document.visualWordList;
			
			EntityScore topicList[] = new EntityScore[numberOfTopic];
			for(int t=0;t<numberOfTopic;t++){
				double count =0;
				for(int w=0;w<textualWordList.size();w++){
					Word word = textualWordList.get(w);
					if(word.topicAssignment==t)
						count++;
				}
				for(int w=0;w<visualWordList.size();w++){
					Word word = visualWordList.get(w);
					if(word.topicAssignment==t)
						count++;
				}
				topicList[t]=new EntityScore(t,count);
			}
			Arrays.sort(topicList);
			int topicID1 = topicList[0].id;
			int topicID2 = topicList[1].id;
			list[topicID1]+=document.documentIndex+";";
			list[topicID2]+=document.documentIndex+";";
		}
		
		BufferedWriter bw = new BufferedWriter(new FileWriter(new File(topicImageFile)));
		for(int t=0;t<list.length;t++){
			bw.write("Topic "+t+" : \n");
			bw.write(list[t]+"\n");
			bw.flush();
		}
		bw.flush();
		bw.close();
	}
	
	public void saveEmotionImage(String emotionImageFile) throws IOException{
		String list[] = new String[numberOfEmotion];
		for(int l=0;l<list.length;l++){
			String str="";
			list[l]=str;
		}
		
		for(int d=0;d<documents.length;d++){
			Document document = documents[d];
			ArrayList<Word> textualWordList = document.textualWordList;
			ArrayList<Word> visualWordList = document.visualWordList;
			EntityScore emotionList[] = new EntityScore[numberOfEmotion];
			for(int e=0;e<emotionList.length;e++){
				double count =0;
				for(int w=0;w<textualWordList.size();w++){
					Word word = textualWordList.get(w);
					if(word.emotionAssignment==e){
						count++;
					}
				}
				for(int w=0;w<visualWordList.size();w++){
					Word word = visualWordList.get(w);
					if(word.emotionAssignment==e){
						count++;
					}
				}
				emotionList[e]= new EntityScore(e,count);
			}
			Arrays.sort(emotionList);
			int emotionID1 = emotionList[0].id;
			int emotionID2 = emotionList[1].id;
			list[emotionID1]+=document.documentIndex+";";
			list[emotionID2]+=document.documentIndex+";";
		}
		
		BufferedWriter bw = new BufferedWriter(new FileWriter(new File(emotionImageFile)));
		for(int e=0;e<list.length;e++){
			bw.write("Emotion "+e+": \n");
			bw.write(list[e]+"\n");
			bw.flush();
		}
		bw.flush();
		bw.close();
	}
	
	public static void main(String args[]) throws IOException{
		String dir ="D:/Projects/ImageProject/result/";
		String documentFile = dir+"document_t15_e3.txt";
		String topicFile=dir+"topic_t15_e3.txt";
		String emotionFile = dir+"emotion_t15_e3.txt";
		String topicImageFile = dir+"topicImage_t15_e3.txt";
		String emotionImageFile=dir+"emotionImage_t15_e3.txt";
		String accuracyFile = dir+"accuracy_t15_e3.txt";

		GibbsSampling gs = new GibbsSampling();
		String file="D:/Projects/ImageProject/ImageSentiment/dataset/text/test.txt";
		String stopFile="D:/Projects/ImageProject/ImageSentiment/dataset/text/stopword.txt";
		String emotionDic="D:/Projects/ImageProject/ImageSentiment/dataset/text/SentimentDictionary.txt";
	
		gs.init(file,stopFile,emotionDic,false,false);
		gs.run(200);
		ModelSave ms = new ModelSave(gs);
		ms.saveDocuments(documentFile);
		ms.saveTopic(topicFile);
		ms.saveEmotion(emotionFile);
		ms.saveTopicImage(topicImageFile);
		ms.saveEmotionImage(emotionImageFile);
		
		Evaluation evaluation = new Evaluation(gs);
		evaluation.getAccuracy(5, documentFile, topicFile, emotionFile, accuracyFile);
		
	}

}

class EntityScore implements Comparable{
	public int id;
	public String entity;
	public double score;
	
	public EntityScore(String entity, double score){
		this.entity=entity;
		this.score=score;
	}
	public EntityScore(int id, double score){
		this.id=id;
		this.score=score;
	}
	
	public int compareTo(Object obj){
		EntityScore es = (EntityScore)obj;
		if(this.score>es.score) return -1;
		else if(this.score==es.score) return 0;
		else return 1;
	}
}
