package algorithm;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;
import java.util.Scanner;

import constants.Constants;
import entity.DataProcess;
import entity.Document;
import entity.Emotion;
import entity.TextualEmotion;
import entity.TextualTopic;
import entity.Topic;
import entity.VisualEmotion;
import entity.VisualTopic;
import entity.Word;

/*
 * This is used to build the model
 * */
public class Model {
	
	public int numberOfTopic;
	public int numberOfEmotion;
	public int numberOfDocument;
	
	public int numberOfTextualTopic;
	public int numberOfVisualTopic;
	public int numberOfTextualEmotion;
	public int numberOfVisualEmotion;
	
	public int numberOfTextualWord;
	public int numberOfVisualWord;
	
	public int numberOfTextualTopicWord;
	public int numberOfTextualEmotionWord;
	public int numberOfVisualTopicWord;
	public int numberOfVisualEmotionWord;
	
	public Topic[] topicList;
	public Emotion[] emotionList;
	public Document[] documents;
	
	public TextualTopic[] textualTopicList;
	public VisualTopic[] visualTopicList;
	
	public TextualEmotion[] textualEmotionList;
	public VisualEmotion[] visualEmotionList;
	
	public HashMap<Integer,String> ID2W;
	public HashMap<String,Integer> W2ID;
	public HashMap<Integer,String> ID2V;
	public HashMap<String,Integer> V2ID;
	
	public double alpha;//for topic distribution per document
	public double gamma; // for sentiment distribution per document
	public double beta1; // for topic text word distribution
	public double beta2; // for topic visual word distribution
	public double beta3;
	public double beta4;
	public double lambada1;
	public double lambada2;
	public double eta;
	public double zeta;
	
	public Model(String documentFile,String stopFile, String emotionFile) throws IOException{
		init(documentFile,stopFile,emotionFile);
	}
	
	public void init(String documentFile,String stopFile,String emotionFile) throws IOException{
		Constants cons = new Constants();
		
		this.alpha=cons.alpha;
		this.beta1=cons.beta1;
		this.beta2=cons.beta2;
		this.beta3=cons.beta3;
		this.beta4=cons.beta4;
		this.gamma=cons.gamma;
		this.lambada1=cons.lambada1;
		this.lambada2=cons.lambada2;
		this.eta=cons.eta;
		this.zeta=cons.zeta;
		
		this.numberOfTopic=cons.numberOfTopic;
		this.numberOfEmotion=cons.numberOfEmotion;
		this.numberOfTextualTopic=cons.numberOfTextualTopic;
		this.numberOfVisualTopic=cons.numberOfVisualTopic;
		this.numberOfTextualEmotion=cons.numberOfTexualEmotion;
		this.numberOfVisualEmotion=cons.numberOfVisualEmotion;
		
		this.numberOfTextualTopicWord=0;
		this.numberOfTextualEmotionWord=0;
		this.numberOfVisualTopicWord=0;
		this.numberOfVisualEmotionWord=0;
		
		DataProcess dp = new DataProcess(documentFile,stopFile,emotionFile);
		this.documents=dp.getDocuments();
		this.ID2W=dp.getID2W();
		this.ID2V=dp.getID2V();
		this.W2ID=dp.getW2ID();
		this.V2ID=dp.getV2ID();
		
		this.numberOfDocument=dp.getNumberOfDocument();
		this.numberOfTextualWord=dp.getNumberOfTextualWord();
		this.numberOfVisualWord= dp.getNumberOfVisualWord();
	
		this.topicList= new Topic[numberOfTopic];
		this.emotionList=new Emotion[numberOfEmotion];
		this.textualTopicList = new TextualTopic[numberOfTextualTopic];
		this.visualTopicList = new VisualTopic[numberOfVisualTopic];
		this.textualEmotionList = new TextualEmotion[numberOfTextualEmotion];
		this.visualEmotionList = new VisualEmotion[numberOfVisualEmotion];
		
		for(int t=0;t<numberOfTopic;t++){
			Topic topic = new Topic(t,numberOfTextualWord,numberOfVisualWord,numberOfTextualWord,numberOfVisualWord);
			topicList[t]=topic;	
		}
		for(int e=0;e<numberOfEmotion;e++){
			Emotion emotion = new Emotion(e,numberOfTextualWord,numberOfVisualWord,numberOfTextualWord,numberOfVisualWord);
			emotionList[e] = emotion;
		}
		
		for(int tw=0;tw<numberOfTextualTopic;tw++){
			TextualTopic tt = new TextualTopic(tw,numberOfTextualWord);
			textualTopicList[tw]=tt;
		}
		
		for(int tv=0;tv<numberOfVisualTopic;tv++){
			VisualTopic vt = new VisualTopic(tv,numberOfVisualWord);
			visualTopicList[tv]=vt;
		}
		
		for(int ew=0;ew<numberOfTextualEmotion;ew++){
			TextualEmotion te = new TextualEmotion(ew,numberOfTextualWord);
			textualEmotionList[ew]=te;
		}
		for(int ev=0;ev<numberOfVisualEmotion;ev++){
			VisualEmotion ve = new VisualEmotion(ev,numberOfVisualWord);
			visualEmotionList[ev]=ve;
		}
		
		//random sample the words at the first time
		Random random = new Random();
		for(int d=0;d<numberOfDocument;d++){
			//process the text
			ArrayList<Word> textualWordList = documents[d].textualWordList;
			for(int t=0;t<textualWordList.size();t++){
				int wordIndex = textualWordList.get(t).wordIndex;
				int wordAttr = textualWordList.get(t).wordAttr;
				if(wordAttr==0){
					int textTopic = random.nextInt(numberOfTextualTopic);
					int topic = random.nextInt(numberOfTopic);
					documents[d].textualWordList.get(t).topicAssignment=topic;
					documents[d].textualWordList.get(t).textualTopicAssignment=textTopic;
					documents[d].textualTopicList[textTopic]++;
					
					textualTopicList[textTopic].wordList[wordIndex]++;
					textualTopicList[textTopic].numberOfWord++;
					
					if(topicList[topic].textualTopicList[textTopic]==0){
						topicList[topic].numberOfTextualTopic++;
					}
					topicList[topic].numberOfTextualWord++;
					topicList[topic].textualTopicList[textTopic]++;
					topicList[topic].textualWordList[wordIndex]++;
					
					this.numberOfTextualTopicWord++;
					
				}else{
					int textEmotion = random.nextInt(numberOfTextualEmotion);
					int emotion = random.nextInt(numberOfEmotion);
					documents[d].textualWordList.get(t).emotionAssignment=emotion;
					documents[d].textualWordList.get(t).textualEmotionAssignment=textEmotion;
					documents[d].textualEmotionList[textEmotion]++;
		
					textualEmotionList[textEmotion].wordList[wordIndex]++;
					textualEmotionList[textEmotion].numberOfWord++;
					
					if(emotionList[emotion].textualEmotionList[textEmotion]==0){
						emotionList[emotion].numberOfTextualEmotion++;
					}
					emotionList[emotion].numberOfTextualWord++;
					emotionList[emotion].textualEmotionList[textEmotion]++;
					emotionList[emotion].textualWordList[wordIndex]++;
					
					this.numberOfTextualEmotionWord++;
				}	
			}
			//process the vision
			ArrayList<Word> visualWordList= documents[d].visualWordList;
			for(int t=0;t<visualWordList.size();t++){
				int wordIndex = visualWordList.get(t).wordIndex;
				int wordAttr = visualWordList.get(t).wordAttr;
				if(wordAttr==0){
					int visualTopic = random.nextInt(numberOfVisualTopic);
					int topic = random.nextInt(numberOfTopic);
					documents[d].visualWordList.get(t).topicAssignment=topic;
					documents[d].visualWordList.get(t).visualTopicAssignment=visualTopic;
					documents[d].visualTopicList[visualTopic]++;
					
					visualTopicList[visualTopic].wordList[wordIndex]++;
					visualTopicList[visualTopic].numberOfWord++;
					
					if(topicList[topic].visualTopicList[visualTopic]==0){
						topicList[topic].numberOfVisualTopic++;
					}
					topicList[topic].numberOfVisualWord++;
					topicList[topic].visualTopicList[visualTopic]++;
					topicList[topic].visualWordList[wordIndex]++;
					
					this.numberOfVisualTopicWord++;
				}else{
					
					int visualEmotion = random.nextInt(numberOfVisualEmotion);
					int emotion= random.nextInt(numberOfEmotion);
					
					documents[d].visualWordList.get(t).emotionAssignment=emotion;
					documents[d].visualWordList.get(t).visualEmotionAssignment=visualEmotion;
					documents[d].visualEmotionList[visualEmotion]++;
					
					visualEmotionList[visualEmotion].wordList[wordIndex]++;
					visualEmotionList[visualEmotion].numberOfWord++;
					
					if(emotionList[emotion].visualEmotionList[visualEmotion]==0){
						emotionList[emotion].numberOfVisualEmotion++;
					}
					emotionList[emotion].numberOfVisualWord++;
					emotionList[emotion].visualEmotionList[visualEmotion]++;
					emotionList[emotion].visualWordList[wordIndex]++;
					
					this.numberOfVisualEmotionWord++;
				}
			}
		}
	}
		
	
	public static void main(String args[]) throws IOException{
		String file="D:/Projects/ImageProject/ImageSentiment/dataset/text/test.txt";
		String stopFile="D:/Projects/ImageProject/ImageSentiment/dataset/text/stopword.txt";
		String emotionFile="D:/Projects/ImageProject/ImageSentiment/dataset/text/SentimentDictionary.txt";
		Model model = new Model(file,stopFile,emotionFile);
	}
}
