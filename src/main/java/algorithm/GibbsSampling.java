package algorithm;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;

import entity.Document;
import entity.Emotion;
import entity.TextualEmotion;
import entity.TextualTopic;
import entity.Topic;
import entity.VisualEmotion;
import entity.VisualTopic;
import entity.Word;

/*
 * This class is used to conduct the Gibbs Sampling
 * */
public class GibbsSampling {
	
	public Model model;
	
	public double alpha; // document topic distribution
	
	public double beta1;// textual topic - word distribution 
	public double beta2; // visual topic - word distribution
	public double beta3;
	public double beta4;
	
	public double gamma ;// document emotion distribution
	public double lambada1;// textual emotion word distribution
	public double lambada2;// visual emotion word distribution
	
	public double eta;// local topic - general topic distribution
	public double zeta;// local emotion - general emotion distribution  
	
	public int numberOfDocument;
	public int numberOfTextualWord;
	public int numberOfVisualWord;
	
	public int numberOfTextualTopic;
	public int numberOfVisualTopic;
	public int numberOfTextualEmotion;
	public int numberOfVisualEmotion;
	public int numberOfTopic;
	public int numberOfEmotion;
	
	public int numberOfTextualTopicWord;
	public int numberOfTextualEmotionWord;
	public int numberOfVisualTopicWord;
	public int numberOfVisualEmotionWord;
	
	public Document[] documents;
	public Topic[] topicList;
	public Emotion[] emotionList;
	public TextualTopic[] textualTopicList;
	public VisualTopic[] visualTopicList;
	public TextualEmotion[] textualEmotionList;
	public VisualEmotion[] visualEmotionList;
	
	private Random random = new Random();
	private double tt[];
	private double vt[];
	private double te[];
	private double ve[];
	private double kt[];
	private double ke[];
	
	private boolean textOnly;
	private boolean visionOnly;
	
	public void init(String documentFile, String stopFile, String emotionFile,boolean textOnly, boolean visionOnly) throws IOException{
		this.model = new Model(documentFile,stopFile, emotionFile);
		
		this.documents=model.documents;
		
		this.alpha=model.alpha;
		this.beta1=model.beta1;
		this.beta2=model.beta2;
		this.beta3=model.beta3;
		this.beta4=model.beta4;
		this.gamma=model.gamma;
		this.lambada1=model.lambada1;
		this.lambada2=model.lambada2;
		this.eta=model.eta;
		this.zeta=model.zeta;
		
		this.numberOfDocument=model.numberOfDocument;
		this.numberOfTextualWord = model.numberOfTextualWord;
		this.numberOfVisualWord = model.numberOfVisualWord;
		
		this.numberOfTopic = model.numberOfTopic;
		this.numberOfEmotion = model.numberOfEmotion;
		this.numberOfTextualTopic = model.numberOfTextualTopic;
		this.numberOfVisualTopic = model.numberOfVisualTopic;
		this.numberOfTextualEmotion= model.numberOfTextualEmotion;
		this.numberOfVisualEmotion=model.numberOfVisualEmotion;
		
		this.numberOfTextualTopicWord=model.numberOfTextualTopicWord;
		this.numberOfTextualEmotionWord = model.numberOfTextualEmotionWord;
		this.numberOfVisualTopicWord = model.numberOfVisualTopicWord;
		this.numberOfVisualEmotionWord = model.numberOfVisualEmotionWord;
		
		
		this.topicList=model.topicList;
		this.emotionList=model.emotionList;
		this.textualTopicList = model.textualTopicList;
		this.textualEmotionList=model.textualEmotionList;
		this.visualTopicList=model.visualTopicList;
		this.visualEmotionList=model.visualEmotionList;
		
		this.tt=new double[numberOfTextualTopic];
		this.vt = new double[numberOfVisualTopic];
		this.te = new double[numberOfTextualEmotion];
		this.ve = new double[numberOfVisualEmotion];
		this.kt = new double[numberOfTopic];
		this.ke = new double[numberOfEmotion];
		
		this.textOnly=textOnly;
		this.visionOnly=visionOnly;
	}
	
	public void run(int iter){
		for(int i=0;i<iter;i++){
			System.out.println("processing "+i+"/"+iter+" sampling...");
			sampling();
		}
	}
	
	private void sampling(){
		for(int d=0;d<documents.length;d++){
			ArrayList<Word> textualWordList = documents[d].textualWordList;
			ArrayList<Word> visualWordList = documents[d].visualWordList;
			if(visionOnly==false){
				for(int t=0;t<textualWordList.size();t++){
					Word word = textualWordList.get(t);
					int wordAttr = word.wordAttr;
					if(wordAttr==0){
						//this is a topic word
						decrementTopic(d,t,true);
						int textualTopic = sampleLocalTopic(d,t,true);
						int topic = sampleTopic(d,t,textualTopic,true);
						allocateTopicWord(d,t,textualTopic, topic,true);
					}else{
						//this is an emotion word
						decrementEmotion(d,t,true);
						int textualEmotion = sampleLocalEmotion(d,t,true);
						int emotion = sampleEmotion(d,t,textualEmotion,true);
						allocateEmotionWord(d,t,textualEmotion,emotion,true);
					}
				}
			}
			
			if(textOnly==false){
				for(int v=0;v<visualWordList.size();v++){
					int wordAttr = visualWordList.get(v).wordAttr;
					if(wordAttr==0){
						decrementTopic(d,v,false);
						int visualTopic= sampleLocalTopic(d,v,false);
						int topic = sampleTopic(d,v,visualTopic,false);
						allocateTopicWord(d,v,visualTopic,topic,false);
					}else{
						decrementEmotion(d,v,false);
						int visualEmotion = sampleLocalEmotion(d,v,false);
						int emotion=sampleEmotion(d,v,visualEmotion,false);
						allocateEmotionWord(d,v,visualEmotion,emotion,false);
					}
				}
			}
		}
	}
	
	private int sampleLocalTopic(int docID, int w, boolean text){
		double pSum=0;
		double u=0;
		
		int topic=-1;
		Document document = documents[docID];
		if(text==true){
			clear(tt);
			int wordIndex = document.textualWordList.get(w).wordIndex;
			for(int t=0;t<textualTopicList.length;t++){
				TextualTopic txt = textualTopicList[t];
				int wordInTopic = textualTopicList[t].wordList[wordIndex];
				int topicInDocument = document.textualTopicList[txt.topicIndex];
				pSum+=((wordInTopic+beta1)/(txt.numberOfWord+numberOfTextualTopicWord*beta1))*((topicInDocument+eta)/(document.numberOfTextualTopicWord+numberOfTextualTopic*eta));
				tt[t]=pSum;
			}
			u=random.nextDouble()*pSum;
			for(int t=0;t<textualTopicList.length;t++){
				if(tt[t]>=u){
					topic=t;
					break;
				}
			}
		}else{
			//visual is true
			clear(vt);
			int wordIndex = document.visualWordList.get(w).wordIndex;
			for(int t=0;t<visualTopicList.length;t++){
				VisualTopic vtc = visualTopicList[t];
				int wordInTopic = visualTopicList[t].wordList[wordIndex];
				int topicInDocument = document.visualTopicList[vtc.topicIndex];
				pSum+=((wordInTopic+beta2)/(vtc.numberOfWord+numberOfVisualTopicWord*beta2))/((topicInDocument+alpha)/(document.numberOfVisualTopicWord+numberOfVisualTopic*alpha));
				vt[t]=pSum;
			}
			u=random.nextDouble()*pSum;
			
			for(int t=0;t<visualTopicList.length;t++){
				if(vt[t]>=u){
					topic=t;
					break;
				}
			}
		}
		return topic;
	}
	
	private int sampleLocalEmotion(int docID,int w, boolean text){
		int emotion=-1;
		
		double pSum=0;
		double u=0;
		
		Document document = documents[docID];
		if(text==true){
			clear(te);
			int wordIndex = document.textualWordList.get(w).wordIndex;
			for(int e=0;e<textualEmotionList.length;e++){
				TextualEmotion tet = textualEmotionList[e];
				int wordInEmotion = textualEmotionList[e].wordList[wordIndex];
				int emotionInDocument = document.textualEmotionList[tet.emotionIndex];
				pSum+=((wordInEmotion+lambada1)/(tet.numberOfWord+numberOfTextualEmotionWord*lambada1))*((emotionInDocument+gamma)/(document.numberOfTextualEmotionWord+numberOfTextualEmotion*gamma));
				te[e]=pSum;
			}
			u=random.nextDouble()*pSum;
			
			for(int e=0;e<textualEmotionList.length;e++){
				if(te[e]>=u){
					emotion=e;
					break;
				}
			}
			
		}else{
			clear(ve);
			int wordIndex = document.visualWordList.get(w).wordIndex;
			for(int e=0;e<visualEmotionList.length;e++){
				VisualEmotion vet = visualEmotionList[e];
				int wordInEmotion = visualEmotionList[e].wordList[wordIndex];
				int emotionInDocument = document.visualEmotionList[vet.emotionIndex];
				pSum+=((wordInEmotion+lambada2)/(vet.numberOfWord+numberOfVisualEmotionWord*lambada2))*((emotionInDocument+gamma)/(document.numberOfVisualEmotionWord+numberOfVisualEmotion*gamma));
				ve[e]=pSum;
			}
			u=random.nextDouble()*pSum;
			
			for(int e=0;e<visualEmotionList.length;e++){
				if(ve[e]>=u){
					emotion=e;
					break;
				}
			}
		}
		return emotion;
	}
	
	private int sampleTopic(int docID, int w, int localTopic, boolean text){
		double pSum=0;
		double u=0;
		int topic=-1;
		clear(kt);
		if(text==true){
			int wordIndex = documents[docID].textualWordList.get(w).wordIndex;
			for(int t=0;t<topicList.length;t++){ 
				int wordInTopic= topicList[t].textualWordList[wordIndex];
				int topicInTopic = topicList[t].textualTopicList[localTopic];
				//System.out.println("Topic "+t+" : "+wordInTopic+" ; "+topicInTopic);
				pSum+=((wordInTopic+beta3)/(topicList[t].numberOfTextualWord+numberOfTextualWord*beta3))*((topicInTopic+eta)/(topicList[t].numberOfTextualTopic+numberOfTopic*gamma));
				kt[t]=pSum;
			}
			u=random.nextDouble()*pSum;
			for(int t=0;t<topicList.length;t++){
				if(kt[t]>=u){
					topic=t;
					break;
				}
			}
			
		}else{
			int wordIndex = documents[docID].visualWordList.get(w).wordIndex;
			
			for(int t=0;t<topicList.length;t++){
				int wordInTopic = topicList[t].visualTopicList[wordIndex];
				int topicInTopic = topicList[t].visualTopicList[localTopic];
				pSum+=((wordInTopic+beta3)/(topicList[t].numberOfVisualWord+numberOfVisualWord*beta3))*((topicInTopic+gamma)/(topicList[t].numberOfVisualTopic+numberOfTopic*gamma));
				kt[t]=pSum;
			}
			pSum=kt[numberOfTopic-1];
			u=random.nextDouble()*pSum;
			for(int t=0;t<topicList.length;t++){
				if(kt[t]>=u){
					topic=t;
					break;
				}
			}
		}
		return topic;
	}
	
	private int sampleEmotion(int docID, int w, int localEmotion,boolean text){
		int emotion=-1;
		double pSum=0;
		double u=0;
		clear(ke);
		if(text==true){
			int wordIndex = documents[docID].textualWordList.get(w).wordIndex;
			for(int e=0;e<emotionList.length;e++){
				int wordInEmotion = emotionList[e].textualWordList[wordIndex];
				int emotionInEmotion = emotionList[e].textualEmotionList[localEmotion];
				pSum+=((wordInEmotion+beta4)/(emotionList[e].numberOfTextualWord+numberOfTextualWord*beta4))*((emotionInEmotion+zeta)/(emotionList[e].numberOfTextualEmotion+numberOfEmotion*zeta));
				ke[e]=pSum;
			}
			u=random.nextDouble()*pSum;
			for(int e=0;e<emotionList.length;e++){
				if(ke[e]>=u){
					emotion=e;
					break;
				}
			}
		}else{
			int wordIndex = documents[docID].visualWordList.get(w).wordIndex;
			for(int e=0;e<emotionList.length;e++){
				int wordInEmotion = emotionList[e].visualWordList[wordIndex];
				int emotionInEmotion = emotionList[e].visualEmotionList[localEmotion];
				pSum+=((wordInEmotion+beta4)/(emotionList[e].numberOfVisualWord+numberOfVisualWord*beta4))*((emotionInEmotion+zeta)/(emotionList[e].numberOfVisualEmotion+numberOfEmotion*zeta));
				ve[e]=pSum;
			}
			u=random.nextDouble()*pSum;
			for(int e=0;e<emotionList.length;e++){
				if(ve[e]>=u){
					emotion=e;
					break;
				}
			}
		}
		return emotion;
	}
	
	
	
	private void decrementTopic(int d, int w, boolean text){
		if(text==true){
			// a text word
			int wordIndex = documents[d].textualWordList.get(w).wordIndex;
			int textualTopicAssignment = documents[d].textualWordList.get(w).textualTopicAssignment;
			int topicAssignment = documents[d].textualWordList.get(w).topicAssignment;
			
			documents[d].textualTopicList[textualTopicAssignment]--;
			
			textualTopicList[textualTopicAssignment].numberOfWord--;
			textualTopicList[textualTopicAssignment].wordList[wordIndex]--;
			
			topicList[topicAssignment].numberOfTextualWord--;
			topicList[topicAssignment].textualTopicList[textualTopicAssignment]--;
			if(topicList[topicAssignment].textualTopicList[textualTopicAssignment]==0){
				topicList[topicAssignment].numberOfTextualTopic--;
			}
			topicList[topicAssignment].textualWordList[wordIndex]--;
			this.numberOfTextualTopicWord--;
		}else{
			// a vision word
			int wordIndex = documents[d].visualWordList.get(w).wordIndex;
			int visualTopicAssignment = documents[d].visualWordList.get(w).visualTopicAssignment;
			int topicAssignment = documents[d].visualWordList.get(w).topicAssignment;
			
			documents[d].visualTopicList[visualTopicAssignment]--;
			
			visualTopicList[visualTopicAssignment].numberOfWord--;
			visualTopicList[visualTopicAssignment].wordList[wordIndex]--;
			
			topicList[topicAssignment].numberOfVisualWord--;
			topicList[topicAssignment].visualTopicList[visualTopicAssignment]--;
			if(topicList[topicAssignment].visualTopicList[visualTopicAssignment]==0){
				topicList[topicAssignment].numberOfVisualTopic--;
			}
			topicList[topicAssignment].visualWordList[wordIndex]--;
			this.numberOfVisualTopicWord--;
		}
		
	}

	private void decrementEmotion(int d, int w, boolean text){
		if(text==true){
			// a text word
			int wordIndex = documents[d].textualWordList.get(w).wordIndex;
			int textualEmotionAssignment = documents[d].textualWordList.get(w).textualEmotionAssignment;
			int emotionAssignment = documents[d].textualWordList.get(w).emotionAssignment;
			
			documents[d].textualEmotionList[textualEmotionAssignment]--;
			
			textualEmotionList[textualEmotionAssignment].numberOfWord--;
			textualEmotionList[textualEmotionAssignment].wordList[wordIndex]--;
			
			emotionList[emotionAssignment].numberOfTextualWord--;
			emotionList[emotionAssignment].textualEmotionList[textualEmotionAssignment]--;
			if(emotionList[emotionAssignment].textualEmotionList[textualEmotionAssignment]==0){
				emotionList[emotionAssignment].numberOfTextualEmotion--;
			}
			emotionList[emotionAssignment].textualWordList[wordIndex]--;
			this.numberOfTextualEmotionWord--;
		}else{
			//a vision word
			int wordIndex=documents[d].visualWordList.get(w).wordIndex;
			int visualEmotionAssignment = documents[d].visualWordList.get(w).visualEmotionAssignment;
			int emotionAssignment = documents[d].visualWordList.get(w).emotionAssignment;
			
			documents[d].visualEmotionList[visualEmotionAssignment]--;
			
			visualEmotionList[visualEmotionAssignment].numberOfWord--;
			visualEmotionList[visualEmotionAssignment].wordList[wordIndex]--;
			
			emotionList[emotionAssignment].numberOfVisualWord--;
			emotionList[emotionAssignment].visualEmotionList[visualEmotionAssignment]--;
			if(emotionList[emotionAssignment].visualEmotionList[visualEmotionAssignment]==0){
				emotionList[emotionAssignment].numberOfVisualEmotion--;
			}
			emotionList[emotionAssignment].visualWordList[wordIndex]--;
			this.numberOfVisualEmotionWord--;
		}
	}
	
	private void allocateTopicWord(int d, int w, int localTopic, int topic, boolean text){
		if(text==true){
			// it is a textual word
			int wordIndex = documents[d].textualWordList.get(w).wordIndex;
			
			documents[d].textualWordList.get(w).textualTopicAssignment=localTopic;
			documents[d].textualWordList.get(w).topicAssignment=topic;
			
			documents[d].textualTopicList[localTopic]++;
			
			textualTopicList[localTopic].wordList[wordIndex]++;
			textualTopicList[localTopic].numberOfWord++;
			
			topicList[topic].numberOfTextualWord++;
			if(topicList[topic].textualTopicList[localTopic]==0){
				topicList[topic].numberOfTextualTopic++;
			}
			topicList[topic].textualTopicList[localTopic]++;
			topicList[topic].textualWordList[wordIndex]++;
			this.numberOfTextualTopicWord++;
		}else{
			// it is a visual word
			int wordIndex = documents[d].visualWordList.get(w).wordIndex;
			
			documents[d].visualWordList.get(w).visualTopicAssignment = localTopic;
			documents[d].visualWordList.get(w).topicAssignment=topic;
			
			documents[d].visualTopicList[localTopic]++;
			
			visualTopicList[localTopic].wordList[wordIndex]++;
			visualTopicList[localTopic].numberOfWord++;
			
			topicList[topic].numberOfVisualWord++;
			if(topicList[topic].visualTopicList[localTopic]==0){
				topicList[topic].numberOfVisualTopic++;
			}
			topicList[topic].visualTopicList[localTopic]++;
			topicList[topic].visualWordList[wordIndex]++;
			this.numberOfVisualTopicWord++;
		}
	}
	
	private void allocateEmotionWord(int d, int w, int localEmotion, int emotion,boolean text){
		if(text==true){
			// it is a text word
			int wordIndex = documents[d].textualWordList.get(w).wordIndex;
			
			documents[d].textualWordList.get(w).textualEmotionAssignment=localEmotion;
			documents[d].textualWordList.get(w).emotionAssignment=emotion;
			
			documents[d].textualEmotionList[localEmotion]++;
			
			textualEmotionList[localEmotion].wordList[wordIndex]++;
			textualEmotionList[localEmotion].numberOfWord++;
			
			emotionList[emotion].numberOfTextualWord++;
			if(emotionList[emotion].textualEmotionList[localEmotion]==0){
				emotionList[emotion].numberOfTextualEmotion++;
			}
			emotionList[emotion].textualEmotionList[localEmotion]++;
			emotionList[emotion].textualWordList[wordIndex]++;
			this.numberOfTextualEmotionWord++;
		}else{
			int wordIndex = documents[d].visualWordList.get(w).wordIndex;
			
			documents[d].visualWordList.get(w).visualEmotionAssignment=localEmotion;
			documents[d].visualWordList.get(w).emotionAssignment=emotion;
			
			documents[d].visualEmotionList[localEmotion]++;
			
			visualEmotionList[localEmotion].wordList[wordIndex]++;
			visualEmotionList[localEmotion].numberOfWord++;
			
			emotionList[emotion].numberOfVisualWord++;
			if(emotionList[emotion].visualEmotionList[localEmotion]==0){
				emotionList[emotion].numberOfVisualEmotion++;
			}
			emotionList[emotion].visualEmotionList[localEmotion]++;
			emotionList[emotion].visualWordList[wordIndex]++;
			this.numberOfVisualEmotionWord++;
		}
	}
	private void clear(double[]arr){
		for(int i=0;i<arr.length;i++){
			arr[i]=0;
		}
	}
	
	
	
//for test
	public static void main(String args[]) throws IOException{
		GibbsSampling gs = new GibbsSampling();
		String file="D:/Projects/ImageProject/ImageSentiment/dataset/text/test.txt";
		String stopFile="D:/Projects/ImageProject/ImageSentiment/dataset/text/stopword.txt";
		String emotionFile="D:/Projects/ImageProject/ImageSentiment/dataset/text/SentimentDictionary.txt";
		gs.init(file,stopFile,emotionFile,false,false);
		gs.run(2000);
		//gs.sampling();
	}
}
