package constants;

public class Constants {
	public static final String dir="D:/Projects/ImageProject/ImageSentiment/dataset/";
	public static final double alpha=0.02;
	public static final double beta1=0.01;
	public static final double beta2=0.01;
	public static final double beta3=0.01;
	public static final double beta4=0.01;
	public static final double gamma=0.01;
	public static final double lambada1 =0.01;
	public static final double lambada2 =0.01;
	public static final double eta=0.02;
	public static final double zeta=0.02;
	
	public static final int numberOfTopic=20;
	public static final int numberOfEmotion=6;
	public static final int numberOfTextualTopic=50;
	public static final int numberOfVisualTopic=50;
	public static final int numberOfTexualEmotion=20;
	public static final int numberOfVisualEmotion=20;
	
}
