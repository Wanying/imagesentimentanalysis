package dataprocess;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.URL;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class ImageDownload {
	
	public void download(String dir) throws SQLException, IOException{
		Connection conn = DataBaseConnection.getConnect();
		String sql = "select id,img_url,keyword from images where id>3157";
		Statement stmt = conn.createStatement();
		ResultSet rs = stmt.executeQuery(sql);
		while(rs.next()){
			int id = rs.getInt("id");
			String imgURL=rs.getString("img_url").trim();
			String type = imgURL.substring(imgURL.lastIndexOf(".")).trim();
			if(type.contains("?")) type=type.substring(0,type.indexOf("?"));
			String keyword = rs.getString("keyword").trim();
			try{
				System.out.println("Downloading "+imgURL+"...");
				URL url = new URL(imgURL);
				BufferedInputStream bis = new BufferedInputStream(url.openStream());
				byte[] bytes = new byte[1024];
				String outFile = dir+id+"-"+keyword+type;
				OutputStream bos = new FileOutputStream(new File(outFile));
				int len;
				while((len=bis.read(bytes))>0){
					bos.write(bytes,0,len);
				}
				bis.close();
				bos.flush();
				bos.close();
			}catch(Exception e){
				System.out.println("Error In Getting Images...");
				e.printStackTrace();
				continue;
			}
		}
		
	}
	
	public static void main(String args[]) throws SQLException, IOException{
		String dir ="D:/Projects/ImageProject/ImageSentiment/dataset/images/";
		ImageDownload id = new ImageDownload();
		id.download(dir);
	}

}
