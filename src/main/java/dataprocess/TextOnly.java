package dataprocess;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

public class TextOnly {
	public static void main(String args[]) throws IOException{
		String inputFile = "D:/Projects/ImageProject/ImageSentiment/dataset/text/test.txt";
		String textFile="D:/Projects/ImageProject/ImageSentiment/dataset/text/textOnly.txt";
		String visionFile = "D:/Projects/ImageProject/ImageSentiment/dataset/text/visionOnly.txt";
		
		File textf = new File(textFile);
		FileWriter textFW = new FileWriter(textf);
		BufferedWriter textBW = new BufferedWriter(textFW);
		
		File visionf = new File(visionFile);
		FileWriter visionFW = new FileWriter(visionf);
		BufferedWriter visionBW = new BufferedWriter(visionFW);
		
		File inf = new File(inputFile);
		FileReader fr = new FileReader(inf);
		Scanner scanner = new Scanner(fr);
		while(scanner.hasNext()){
			String line = scanner.nextLine().trim();
			String terms[]= line.split(":");
			String id = terms[0].trim();
			String content = terms[1].trim();
			
			String items[]=content.split(";");
			String text=items[0].trim();
			String vision = items[1].trim();
			
			textBW.write(id+":"+text+"; ");
			textBW.newLine();
			textBW.flush();
			
			visionBW.write(id+": ;"+vision);
			visionBW.newLine();
			visionBW.flush();
		}
		
		textBW.flush();
		textBW.close();
		
		visionBW.flush();
		visionBW.close();
	}

}
