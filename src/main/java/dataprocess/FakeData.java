package dataprocess;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Random;

/**
 * This class is used to generate fake data for test
 * */
public class FakeData {
	
	public void randomFakeData(String outFile) throws IOException{
		File f = new File(outFile);
		FileWriter fw = new FileWriter(f);
		BufferedWriter bw = new BufferedWriter(fw);
		
		//fake text word
		Random random = new Random();
		
		int length = random.nextInt(50);
		int n=0;
		
		while(n<1000){
			String textStr ="";
			for(int t=0;t<length;t++){
				int entity = random.nextInt(20);
				textStr+=entity+" ";
			}
			
			textStr=textStr.trim();
			
			String visualStr="";
			for(int v=0;v<length;v++){
				int entity=random.nextInt(30);
				visualStr+=entity+" ";
			}
			visualStr=visualStr.trim();
			
			String line = textStr+";"+visualStr;
			bw.write(line);
			bw.newLine();
			bw.flush();
			n++;
		}
		
		bw.flush();
		bw.close();
		
	}
	
	public static void main(String args[]) throws IOException{
		FakeData fd = new FakeData();
		String outFile="D:/Projects/ImageProject/ImageSentiment/dataset/fake_data.txt";
		fd.randomFakeData(outFile);
	}

}
