package dataprocess;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.Scanner;

public class PictureFeatureProcess {
	
	public double[][] matrix;
	public int numberOfFiles;
	public int numberOfDimension;
	public int FileLength;
	public File[] fileList;
	
	public PictureFeatureProcess(String fileDir,String filterKeyword,String outputFile) throws IOException{
		getFiles(fileDir,filterKeyword);
		System.out.println("Begin to process Matrix");
		getMatrix();
		System.out.println("Begin to write");
		writeDocument(outputFile);
		
	}
	
	public void getFiles(String fileDir,String filterKeyword) throws IOException{
		File file = new File(fileDir);
		FeatureFileFilter filter = new FeatureFileFilter(filterKeyword);
		this.fileList = file.listFiles(filter);
		String lastName = fileList[fileList.length-1].getName().trim();
		int lastNum = Integer.parseInt(lastName.substring(0,lastName.indexOf("-")).trim());
		
		this.numberOfFiles=lastNum;
		this.FileLength=fileList.length;
		
		System.out.println("The Size Of Files: "+this.FileLength);
		
		File firstFile = fileList[0];
		FileReader fr = new FileReader(firstFile);
		Scanner scanner = new Scanner(fr);
		int n=0;
		while(scanner.hasNext()){
			String line = scanner.nextLine().trim();
			if(line.isEmpty()) break;
			n++;
		}
		numberOfDimension=n;
		matrix = new double[numberOfFiles][numberOfDimension];
	}
	
	public void getMatrix() throws IOException{
		for(int f=0;f<fileList.length;f++){
			File file = fileList[f];
			String fileName = file.getName();
			int id = Integer.parseInt(fileName.substring(0,fileName.indexOf("-")).trim());
			FileReader fr = new FileReader(file);
			Scanner scanner = new Scanner(fr);
			int dimension=0;
			while(scanner.hasNext()){
				String line = scanner.nextLine().trim();
				if(line.isEmpty()) break;
				Double score = Double.parseDouble(line);
				matrix[id-1][dimension]=score;
				dimension++;
			}
		}
	}
	
	public void writeDocument(String outputFile) throws IOException{
		BufferedWriter writer = new BufferedWriter(new FileWriter(new File(outputFile)));
		double[] average = new double[this.numberOfDimension];
		for(int i=0;i<matrix.length;i++){
			for(int j=0;j<matrix[i].length;j++){
				average[j]+=matrix[i][j]/this.FileLength;
			}
		}
		
		for(int i=0;i<matrix.length;i++){
			double sum=sumUp(matrix[i]);
			if(sum==0.0) continue;
			writer.write((i+1)+" : ");
			for(int j=0;j<matrix[i].length;j++){
				if(matrix[i][j]>average[j]){
					writer.write("1\t");
				}else{
					writer.write("0\t");
				}
			}
			writer.newLine();
			writer.flush();
		}
		writer.flush();
		writer.close();
	}
	
	public double sumUp(double arr[]){
		double sum=0.0;
		
		for(int i=0;i<arr.length;i++){
			sum+=arr[i];
		}
		return sum;
	}
	
	public static void main(String args[]) throws IOException{
		String dir="D:/Projects/ImageProject/ImageProject/features/texton_txt/";
		String filterKeyword="l2";
		String output="D:/Projects/ImageProject/ImageSentiment/dataset/vision/texton_l2.txt";
		
		PictureFeatureProcess pfp = new PictureFeatureProcess(dir,filterKeyword,output);
	}
	

}
