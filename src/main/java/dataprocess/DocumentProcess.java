package dataprocess;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Scanner;
import java.util.Map.Entry;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class DocumentProcess {
	
	public HashSet<String> stopList= new HashSet<String>();
	public HashMap<Integer,String> docMap = new HashMap<Integer,String>();
	
	public void readDocuments() throws SQLException{
		DataBaseConnection dbc = new DataBaseConnection();
		Connection conn = dbc.getConnect();
		Statement stmt = conn.createStatement();
		String sql = "select id, title, text, tags from images ";
		ResultSet rs = stmt.executeQuery(sql);
		Pattern pattern = Pattern.compile("[^a-zA-Z0-9\\s ]");
		while(rs.next()){
			String idstr = rs.getString("id");
			int id = Integer.parseInt(idstr.trim());
			
			String title = rs.getString("title");
			Matcher titleMatcher = pattern.matcher(title);
			while(titleMatcher.find()){
				String m = titleMatcher.group();
				title =title.replace(m, " ");
			}
			title=title.replaceAll("( )+",  " ");
			title=title.toLowerCase();
			
			String text = rs.getString("text").trim();
			text =text.toLowerCase();
			
			String tags = rs.getString("tags").trim();
			tags =tags.replaceAll(";", " ");
			tags = tags.toLowerCase();
			
			String doc=title+" "+text+" "+tags+";";
			docMap.put(id, doc);
		}
		//System.out.println("Document Size: "+docMap.size());
		
	}
	
	public void combineDocument(String visionDir) throws IOException{
		File visionFileDir = new File(visionDir);
		File[] visionFileList = visionFileDir.listFiles();
		for(int f=0;f<visionFileList.length;f++){
			File file = visionFileList[f];
			String name = file.getName();
			System.out.println("Is Processing File "+name);
			name = name.substring(0,name.indexOf("."));
			FileReader fileReader = new FileReader(file);
			Scanner scanner = new Scanner(fileReader);
			while(scanner.hasNext()){
				String line = scanner.nextLine().trim();
				String segments[] = line.split(":");
				
				String idstr = segments[0].trim();
				int id = Integer.parseInt(idstr.trim());
				
				String features = segments[1].trim();
				String terms[]=features.split("\t");
				String visionVector="";
				for(int t=0;t<terms.length;t++){
					String term = terms[t].trim();
					if(term.equals("0")) continue;
					if(term.equals("1")){
						String visionWord = name+"_"+t+" ";
						visionVector+=visionWord;
					}
				}
				String doc=docMap.get(id);
				doc+=visionVector;
				docMap.put(id, doc);
			}
		}
	}
	
	public void writeDocuments(String file) throws IOException{
		File outFile = new File(file);
		FileWriter fw = new FileWriter(outFile);
		BufferedWriter bw = new BufferedWriter(fw);
		
		Iterator iter= docMap.entrySet().iterator();
		System.out.println("Document Size: "+docMap.size());
		while(iter.hasNext()){
			Entry entry = (Entry)iter.next();
			String id = entry.getKey().toString();
			String content = entry.getValue().toString().trim();
			if(content.endsWith(";")) continue;
			
			bw.write(id+" : "+content);
			//System.out.println(id+" : "+content);
			bw.newLine();
			bw.flush();
		}

		bw.flush();
		bw.close();
	}
	
	
	public static void main(String args[]) throws IOException, SQLException{
		DocumentProcess dp = new DocumentProcess();
		String stopFile = "D:/Projects/ImageProject/ImageSentiment/dataset/stopword.txt";
		String output = "D:/Projects/ImageProject/ImageSentiment/dataset/text/data.txt";
		String visionDir="D:/Projects/ImageProject/ImageSentiment/dataset/vision/";
		dp.readDocuments();
		System.out.println("Finishing Reading Documents....");
		dp.combineDocument(visionDir);
		System.out.println("Finishing Combining Documents...");
		dp.writeDocuments(output);
	}

}
