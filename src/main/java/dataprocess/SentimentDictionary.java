package dataprocess;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

public class SentimentDictionary {
	
	public static void main(String args[]) throws IOException{
		String inputFile = "D:/Projects/ImageProject/ImageSentiment/dataset/sentiment/MQPA.tff";
		String outputFile ="D:/Projects/ImageProject/ImageSentiment/dataset/sentiment/SentimentDictionary.txt";
		
		File outf = new File(outputFile);
		FileWriter fw = new FileWriter(outf);
		BufferedWriter bw = new BufferedWriter(fw);
		
		File inf = new File(inputFile);
		FileReader fr = new FileReader(inf);
		Scanner scanner = new Scanner(fr);
		while(scanner.hasNext()){
			String line = scanner.nextLine().trim();
			String terms[] = line.split(" ");
			String term= terms[2];
			String items[] = term.split("=");
			String word = items[1].trim();
			word = word.toLowerCase();
			bw.write(word);
			bw.newLine();
			bw.flush();
		}
		bw.flush();
		bw.close();
	}
	

}
