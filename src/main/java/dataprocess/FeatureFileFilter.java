package dataprocess;

import java.io.File;
import java.io.FilenameFilter;

public class FeatureFileFilter implements FilenameFilter{
	
	private String keyword;
	public FeatureFileFilter(String keyword){
		this.keyword=keyword;
	}

	public boolean accept(File dir, String name) {
		name = name.toLowerCase();
		if(name.contains(keyword)) return true;
		else return false;
	}
	
	

}
