package dataprocess;

import java.sql.Connection;
import java.sql.DriverManager;

public class DataBaseConnection {
	
	public static Connection getConnect(){
		Connection  conn =null;
		
		try{
			Class.forName("com.mysql.jdbc.Driver").newInstance();
			conn=DriverManager.getConnection("jdbc:mysql://192.168.1.55:3306/flickr","root","111111");
			System.out.println("Successfully connect to the database");
		}catch(Exception e){
			System.out.println(e.getMessage());
			e.printStackTrace();
		}
		
		return conn;
		
	}
	
	public static void main(String args[]){
		DataBaseConnection dbc = new DataBaseConnection();
		dbc.getConnect();
	}

}
